<?php
/**
 * @package                Joomla.Site
 * @subpackage        Templates.beez5
 * @copyright        Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');

// check modules
$showRightColumn        = ($this->countModules('position-3') or $this->countModules('position-6') or $this->countModules('position-8'));
$showbottom                        = ($this->countModules('position-9') or $this->countModules('position-10') or $this->countModules('position-11'));
$showleft                        = ($this->countModules('position-4') or $this->countModules('position-7') or $this->countModules('position-5'));

if ($showRightColumn==0 and $showleft==0) {
        $showno = 0;
}

JHtml::_('behavior.framework', true);

// get params
$color                        = $this->params->get('templatecolor');
$logo                        = $this->params->get('logo');
$navposition        = $this->params->get('navposition');
$app                        = JFactory::getApplication();
$doc                        = JFactory::getDocument();
$templateparams        = $app->getTemplate(true)->params;

/*
 * Bootstrap include
 * Add JavaScript Frameworks
 */
JHtml::_('bootstrap.framework');
JHtml::_('bootstrap.loadCss');

//$doc->addScript($this->baseurl.'/templates/'.$this->template.'/javascript/md_stylechanger.js', 'text/javascript', true);
?>
<?php if(!$templateparams->get('html5', 0)): ?>
<!DOCTYPE html>
<?php else: ?>
        <?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html lang="de">
        <head>
                <jdoc:include type="head" />

                <meta name="viewport" content="width=device-width, initial-scale=1">

                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/position.css" type="text/css" media="screen,projection" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/layout.css" type="text/css" media="screen,projection" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/print.css" type="text/css" media="Print" />
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/beez5.css" type="text/css" />

<?php
        $files = JHtml::_('stylesheet', 'templates/'.$this->template.'/css/general.css', null, false, true);
        if ($files):
                if (!is_array($files)):
                        $files = array($files);
                endif;
                foreach($files as $file):
?>
                <link rel="stylesheet" href="<?php echo $file;?>" type="text/css" />
<?php
                 endforeach;
        endif;
?>
                <?php if ($this->direction == 'rtl') : ?>
                <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/template_rtl.css" type="text/css" />
                <?php endif; ?>
                <!--[if lte IE 6]>
                        <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ieonly.css" rel="stylesheet" type="text/css" />
                <![endif]-->
                <!--[if IE 7]>
                        <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ie7only.css" rel="stylesheet" type="text/css" />
                <![endif]-->
<?php if($templateparams->get('html5', 0)) { ?>
                <!--[if lt IE 9]>
                        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/html5.js"></script>
                <![endif]-->
<?php } ?>

            <!-- CG 089 -->
            <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/jquery.mmenu.all.css" type="text/css" />
            <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/overrides.css" type="text/css" />
            <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/responsive.css" type="text/css" />

                <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/hide.js"></script>
                <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/javascript/jquery.mmenu.min.all.js"></script>

<title></title>
<?php /*
    <script type='text/javascript'>
        (function (d, t) {
              var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
              bh.type = 'text/javascript';
              bh.src = '//www.bugherd.com/sidebarv2.js?apikey=hmchsrzl2pi4m27rgaggea';
              s.parentNode.insertBefore(bh, s);
              })(document, 'script');
    </script>
*/ ?>
</head>
<body id="itemId-<?php print $itemid ?>" class="site<?php print $body_class . $detectAgent . ($clientMobile ? "mobile" : " "); ?>">

<div id="page">
    <div id="all" class="container-fluid">
            <div id="back">
                    <div id="header">
                            <div class="row-fluid"> 
                                
                                <?php if ($this->countModules('mobile_header')): ?>
                                    <jdoc:include type="modules" name="mobile_header" style="custom" />
                                <?php endif; ?>

                                        <div id="header-image" class="span7">
                                            
                                                <jdoc:include type="modules" name="position-15" style="custom" />

                                        </div>
                                             <div id="line" class="span5 mobile-line-id">
                                                <jdoc:include type="modules" name="position-0" style="custom"/>
                                            </div> <!-- end line -->

                                        <div class="clr"></div>

                                            <?php if (!$templateparams->get('html5', 0)): ?>

                                        <?php if($detect->isMobile()) :?>
                                            <div class="header" style="display: table-row;">                                         
                                                <div class="mobile-h3" style="display: table-cell;">                                                  
                                                   
                                                        <a href="#menu" class="btn-menu">
                                                            <div id="toggle" class="button_container">
                                                                <span class="top"></span>
                                                                <span class="middle"></span>
                                                                <span class="bottom"></span>
                                                            </div>
                                                        </a>
                                                                                                 
                                                </div>                                                    
                                                 <div class="mobile-service">                                                        
                                                        <h3>Service-Telefon: </h3>                                                        
                                                </div>
                                                <div class="mobile-service">
                                                    <p style="display: table-cell;"><a href="tel:+49814190814"> 08141 - 908 14</a></p>
                                                </div>                                    
                                            <nav id="menu">
                                                <jdoc:include type="modules" name="mobile-menu" />
                                            </nav> <!-- end mobile Menue -->
                                                <script>
                                                    jQuery('#toggle').click( function() {
                                                            $(this).toggleClass('active');                                                               
                                                        });
                                                </script>
                                        <?php endif; ?> 
                                
                                    </div>
                            </div>
                    </div><!-- end div header -->
                                    <?php else: ?>
                                    <?php endif; ?>

                                    <div id="<?php echo $showRightColumn ? 'contentarea2' : 'contentarea'; ?>" class="content">
                                        <div id="breadcrumbs">
                                            <jdoc:include type="modules" name="position-2" />
                                        </div>
                                            <?php if ($navposition=='left' and $showleft) : ?>

                                                    <?php if(!$this->params->get('html5', 0)): ?>
                                                            <div class="left1 <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav">
                                                    <?php else: ?>
                                                            <nav class="left1 <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav">
                                                    <?php endif; ?>

                                                                    <jdoc:include type="modules" name="position-7" style="beezDivision" headerLevel="3" />
                                                                    <jdoc:include type="modules" name="position-4" style="beezHide" headerLevel="3" state="0 " />
                                                                    <jdoc:include type="modules" name="position-5" style="beezTabs" headerLevel="2"  id="3" />

                                                    <?php if(!$this->params->get('html5', 0)): ?>
                                                            </div><!-- end navi -->
                                                    <?php else: ?>
                                                            </nav>
                                                    <?php endif; ?>

                                            <?php endif; ?>

                                            <div id="<?php echo $showRightColumn ? 'wrapper' : 'wrapper2'; ?>" 
                                                <?php print (isset($showno)) ? 'class="shownocolumns span12"' : 'class="container-fluid"';?>>

                                                    <div id="main" class="row">
                                                    <?php if(!$detect->isMobile()): ?>
                                                        <?php if ($this->countModules('position-12')): ?>
                                                                <div id="top"><jdoc:include type="modules" name="position-12"   />
                                                                </div>
                                                        <?php endif; ?>

                                                                <jdoc:include type="message" />
                                                                <jdoc:include type="component" />
                                                    <?php else : ?>
                                                        <div class="span7 mobile-component">                                                
                                                                <jdoc:include type="message" />
                                                                <jdoc:include type="component" />
                                                        </div>
                                                    <?php endif; ?>
                                                        <?php if($detect->isMobile()) : ?>
                                                            <div id="right" class="span5 right-mobile">
                                                                <a id="additional"></a>
                                                                <jdoc:include type="modules" name="position-6" style="beezDivision" headerLevel="3"/>
                                                                <jdoc:include type="modules" name="position-8" style="beezDivision" headerLevel="3"  />
                                                                <jdoc:include type="modules" name="position-3" style="beezDivision" headerLevel="3"  />
                                                            </div><!-- end right -->
                                                        <?php endif; ?>

                                                    </div><!-- end main -->

                                            </div><!-- end wrapper -->

                                    <?php if ($showRightColumn) : ?>
                                            <div id="close">
                                                    <a href="#" onclick="auf('right')">
                                                            <span id="bild">
                                                                    <?php echo JText::_('TPL_BEEZ5_TEXTRIGHTCLOSE'); ?>
                                                            </span>
                                                    </a>
                                            </div>

                                    <?php if(!$detect->isMobile()) {
                                            if (!$templateparams->get('html5', 0)): ?>
                                                <div id="right">
                                        <?php else: ?>
                                                <aside id="right">
                                        <?php endif; ?>
                                                    <a id="additional"></a>
                                                    <jdoc:include type="modules" name="position-6" style="beezDivision" headerLevel="3"/>
                                                    <jdoc:include type="modules" name="position-8" style="beezDivision" headerLevel="3"  />
                                                    <jdoc:include type="modules" name="position-3" style="beezDivision" headerLevel="3"  />
                                        <?php if(!$templateparams->get('html5', 0)): ?>
                                                </div><!-- end right -->
                                        <?php else: ?>
                                                </aside>
                                        <?php endif; ?>
                                    <?php } ?>

                            <?php endif; ?>

                            <?php if ($navposition=='center' and $showleft) : ?>

                                    <?php if (!$this->params->get('html5', 0)): ?>
                                            <div class="left <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav" >
                                    <?php else: ?>
                                            <nav class="left <?php if ($showRightColumn==NULL){ echo 'leftbigger';} ?>" id="nav">
                                    <?php endif; ?>

                                                    <jdoc:include type="modules" name="position-7"  style="beezDivision" headerLevel="3" />
                                                    <jdoc:include type="modules" name="position-4" style="beezHide" headerLevel="3" state="0 " />
                                                    <jdoc:include type="modules" name="position-5" style="beezTabs" headerLevel="2"  id="3" />

                                    <?php if (!$templateparams->get('html5', 0)): ?>
                                            </div><!-- end navi -->
                                    <?php else: ?>
                                            </nav>
                                    <?php endif; ?>
                            <?php endif; ?>

                                            <div class="wrap"></div>

                                    </div> <!-- end contentarea -->

                            </div><!-- back -->

                    </div><!-- all -->

                    <?php if(!$detect->isMobile()) : ?>
                        <div id="footer-outer">

                    <?php if ($showbottom) : ?>
                            <div id="footer-inner">

                                    <div id="bottom">
                                            <?php if ($this->countModules('position-9')): ?>
                                            <div class="box box1"> <jdoc:include type="modules" name="position-9" style="beezDivision" headerlevel="3" /></div>
                                            <?php endif; ?>
                                               <?php if ($this->countModules('position-10')): ?>
                                            <div class="box box2"> <jdoc:include type="modules" name="position-10" style="beezDivision" headerlevel="3" /></div>
                                            <?php endif; ?>
                                            <?php if ($this->countModules('position-11')): ?>
                                            <div class="box box3"> <jdoc:include type="modules" name="position-11" style="beezDivision" headerlevel="3" /></div>
                                            <?php endif ; ?>
                                    </div>
                            </div>
                    <?php endif ; ?>

                            <div id="footer-sub">

                            <?php if (!$templateparams->get('html5', 0)): ?>
                                    <div id="footer">
                            <?php else: ?>
                                    <footer id="footer">
                            <?php endif; ?>

                                            <jdoc:include type="modules" name="position-14" />
                                            <p>
                                                    &copy; <?php echo date("Y");?> Kaminofen München - Beratung, Planung, Einbau / Holzbriketts und Pelletts - Hausservice Probst www.ofen-dahoam.de
                                            </p>             <jdoc:include type="modules" name="impressum" />



                            <?php if (!$templateparams->get('html5', 0)): ?>
                                    </div><!-- end footer -->
                            <?php else: ?>
                                    </footer>
                            <?php endif; ?>

                            </div>

                    </div>
                    <?php else: ?>
                        <div class="copyright-mobile">                
                            <p>&copy; <?php echo date("Y");?> Kaminofen München - Beratung, Planung, Einbau / Holzbriketts und Pelletts - Hausservice Probst www.ofen-dahoam.de</p> 
                        </div>
                      <?php endif; ?>    
                <jdoc:include type="modules" name="debug" />
            </div> <!-- page -->                

<!--mmenu  -->                
            <script type="text/javascript">

            // variables
            var $btnMenu = $('.btn-menu');
            var $img = $('img');

             jQuery(document).ready(function( $ ) {
                $("#menu").mmenu({
                       "extensions": [
                          "effect-menu-zoom",
                          "effect-listitems-slide",
                          null,
                          "pageshadow"
                       ],
                        navbar: {
                        title: "Ofen dahoam Menü"
                    },
                       offCanvas: {
                        position  : "left",
                        zposition : "back"
                    }

                });
                // toggle menu
               /* var api = $('#menu').data("mmenu");

                $btnMenu.click(function() {
                  api.open();
                });

                // callbacks
                api.bind('opening', function() {
                  $img.attr('src', '<?php print $baseUrl?>images/arrows_remove.svg'); <?php // $baseUrl ->siehe template functions?>
                });

                api.bind('closing', function() {
                  $img.attr('src', '<?php print $baseUrl?>images/arrows_hamburger.svg');
                });*/

                // change toggle behavior for subpanels
                $('#menu').find( ".mm-next" ).addClass("mm-fullsubopen");

             });

            </script> 
        </body>
</html>
