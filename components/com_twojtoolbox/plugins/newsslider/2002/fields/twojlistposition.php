<?php
/**
* @package 2JToolBox 2JNewsSlider
* @Copyright (C) 2012 2Joomla.net
* @ All rights reserved
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
* @version $Revision: 2.0.0 $
**/


defined('JPATH_BASE') or die;
jimport('joomla.html.html');
jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('list');
class JFormFieldTwoJListPosition extends JFormFieldTwoJList{
	protected $type = 'TwoJListPosition';
	
	protected function getOptions(){
		$options = array();
		$options[] = JHtml::_('select.option', '-', 'Hide');
		
		$options[] = JHtml::_('select.optgroup', 				JText::_('Outside Top'));
		$options[] = JHtml::_('select.option', 'leftbefore', 	JText::_('Outside top left'));
		$options[] = JHtml::_('select.option', 'centerbefore', 	JText::_('Outside top center'));
		$options[] = JHtml::_('select.option', 'rightbefore', 	JText::_('Outside top right'));
		$options[] = JHtml::_('select.optgroup', 				JText::_('Outside Top'));
		
		$options[] = JHtml::_('select.optgroup', 				JText::_('Top'));
		$options[] = JHtml::_('select.option', 'lefttop', 		JText::_('Top left'));
		$options[] = JHtml::_('select.option', 'centertop', 	JText::_('Top centre'));
		$options[] = JHtml::_('select.option', 'righttop', 		JText::_('Top right'));
		$options[] = JHtml::_('select.optgroup', 				JText::_('Top'));
		
		$options[] = JHtml::_('select.optgroup', 				JText::_('Centre'));
		$options[] = JHtml::_('select.option', 'leftcentre', 	JText::_('Centre left'));
		$options[] = JHtml::_('select.option', 'rightcentre', 	JText::_('Centre right'));
		$options[] = JHtml::_('select.optgroup', 				JText::_('Centre'));
		
		$options[] = JHtml::_('select.optgroup', 				JText::_('Bottom'));
		$options[] = JHtml::_('select.option', 'leftbottom', 	JText::_('Bottom left'));
		$options[] = JHtml::_('select.option', 'centerbottom', 	JText::_('Bottom centre'));
		$options[] = JHtml::_('select.option', 'rightbottom', 	JText::_('Bottom right'));
		$options[] = JHtml::_('select.optgroup', 				JText::_('Bottom'));
		
		$options[] = JHtml::_('select.optgroup', 				JText::_('Outside bottom'));
		$options[] = JHtml::_('select.option', 'leftafter', 	JText::_('Outside bottom left'));
		$options[] = JHtml::_('select.option', 'centerafter', 	JText::_('Outside bottom centre'));
		$options[] = JHtml::_('select.option', 'rightafter', 	JText::_('Outside bottom right'));
		$options[] = JHtml::_('select.optgroup', 				JText::_('Outside bottom'));
		
		
		$options = array_merge(parent::getOptions(), $options);
		return $options;
	}
}
