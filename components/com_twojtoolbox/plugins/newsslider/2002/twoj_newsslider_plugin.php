<?php
/**
* @package 2JToolBox 2JNewsSlider
* @Copyright (C) 2012 2Joomla.net
* @ All rights reserved
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html
* @version $Revision: 2.0.0 $
**/

defined('_JEXEC') or die;

class TwoJToolBoxNewsSlider extends TwoJToolBoxPlugin{
	protected $css_list=array('newsslider');
	protected $js_list=array('newsslider.e', 'newsslider');
	protected $delete_color_char = 0;
	
	protected $slider_html = '';
	protected $content_plugin = 1;
	protected $browser;
	protected $zindex;
	protected $menu = '';
	
	protected $block_class = '';
	
	protected $content_template = '';
	
	protected $top_pad = 0;		
	protected $left_pad = 0;		
	protected $right_pad = 0; 		
	protected $bottom_pad = 0;
	
	protected $leftbefore = '';	protected $centerbefore = '';	protected $rightbefore = '';
	protected $lefttop = '';	protected $centertop = '';		protected $righttop = '';
	protected $leftcentre = '';	protected $rightcentre = '';
	protected $leftbottom = '';	protected $centerbottom = '';	protected $rightbottom = '';
	protected $leftafter = '';	protected $centerafter = '';	protected $rightafter = '';
	
	public $contentImages = 0;
	
	//24.04.2013
	protected $date_format = '';
	
	
	public function includeLib(){

		$this->contentImages = $this->getInt('contentImages', 0);
		if($this->contentImages==2) $this->js_list[] = 'imageresize';
		
		$css_file = $this->getString('css_file', '2j.newsslider_theme1.css');
		if($css_file){
			$css_file = preg_replace ("/^2j\.(.*)\.css$/", "$1", $css_file);
			$this->css_list[] = $css_file;
			$this->block_class .= ' twoj_'.$css_file;
			parent::includeLib();
		}
	}
	
	function setPosition( $name, $str = '' ){
		$value = $this->getString($name);
		if($value=='-') $value = '';
		if( $name=='menu' && !$value) $value = 'centerbottom';
		if($value) $this->$value .= $str ? $str : '<div id="twoj_slider_button_'.$name.'_id'.$this->uniqueid.'" class="twoj_slider_button twoj_slider_button_'.$name.'" style="z-index:'.($this->zindex+8).';">'.$this->getString($name.'_title', $name).'</div>';
		if($name=='play' && $value){
			$name = 'stop';
			$this->$value .= '<div id="twoj_slider_button_'.$name.'_id'.$this->uniqueid.'" class="twoj_slider_button twoj_slider_button_'.$name.'" style="z-index:'.($this->zindex+8).'; display:none;">'.$this->getString($name.'_title', $name).'</div>';
		}
	}	
	
	function getActivePosition($name){
		if($this->$name){
			$this->block_class .= ' twoj_slider_block_contain_'.$name;
			return '<div id="twoj_slider_position_'.$name.'_id'.$this->uniqueid.'" class="twoj_slider_position_'.$name.' twoj_slider_position_block" style="visibility: hidden; z-index:'.($this->zindex+7).';">'.$this->$name.'</div>';
		}
	}
	
	function getActivePositions(){
		$return_html = '';
		$return_html .= $this->getActivePosition('leftbefore');
		$return_html .= $this->getActivePosition('centerbefore');
		$return_html .= $this->getActivePosition('rightbefore');
		
		$return_html .= $this->getActivePosition('lefttop');
		$return_html .= $this->getActivePosition('centertop');
		$return_html .= $this->getActivePosition('righttop');
		
		$return_html .= $this->getActivePosition('leftcentre');
		$return_html .= $this->getActivePosition('rightcentre');
		
		$return_html .= $this->getActivePosition('leftbottom');
		$return_html .= $this->getActivePosition('centerbottom');
		$return_html .= $this->getActivePosition('rightbottom');
		
		$return_html .= $this->getActivePosition('leftafter');
		$return_html .= $this->getActivePosition('centerafter');
		$return_html .= $this->getActivePosition('rightafter');

		if($return_html) return $return_html;
	}
	function getGradient($name_start_color, $name_end_color){
			$bg_color_start = $this->getColor($name_start_color);
			$bg_color_end = $this->getColor($name_end_color);
			$ret_html = '';
			if($bg_color_start && $bg_color_start!='transperent' && $name_end_color && $name_end_color!='transperent'){
				$ret_html .='background: -webkit-gradient(linear, left top, left bottom, from('.$bg_color_start.'), to('.$bg_color_end .'));';
				$ret_html .='background: -moz-linear-gradient(top,  '.$bg_color_start.',  '.$bg_color_end .');';
				$ret_html .='background: -o-linear-gradient(top,  '.$bg_color_start.',  '.$bg_color_end .');';
				$ret_html .='background: -ms-linear-gradient(top,  '.$bg_color_start.',  '.$bg_color_end .');';
				$ret_html .='filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\''.$bg_color_start.'\', endColorstr=\''.$bg_color_end .'\');';
			}
			return $ret_html;
	}
	
	public function getElement(){
	
		$db	= JFactory::getDBO();
		$app = JFactory::getApplication();
		jimport( 'joomla.environment.browser' );
		$this->browser = JBrowser::getInstance();
		
		if($this->multitag==0){
			$this->list = $this->getList();
			$this->grouped = false;
			$article_grouping = $this->params->get('article_grouping', 'none');
			$article_grouping_direction = $this->params->get('article_grouping_direction', 'ksort');
			$item_heading = $this->params->get('item_heading');
			if ($article_grouping !== 'none'){
				$this->grouped = true;
				switch($article_grouping){
					case 'year':
					case 'month_year':
						$this->list = TwojToolBoxSiteHelper::contentGroupByDate($this->list, $article_grouping, $article_grouping_direction, $this->params->get('month_year_format', 'F Y'));
						break;
					case 'author':
					case 'category_title':
						$this->list = TwojToolBoxSiteHelper::contentGroupBy($this->list, $article_grouping, $article_grouping_direction);
						break;
					default:
						break;
				}
			}
		}
		$this->uniqueid = $this->getuniqueid();
		$this->addGenOption("uniqid : ".$this->uniqueid);
		$return_text = '';
		
				
		$this->date_format = $this->getString('date_format',  '');
		if( !$this->date_format) $this->date_format = JText::_('DATE_FORMAT_LC2');
		
		$this->slider_html = $this->articles_diplay();
		if($this->contentImages==1){
			$this->slider_html = preg_replace('/<img(?:\\s[^<>]*)?>/i', '', $this->slider_html);
		}
		$width = $this->getSize('width');
		$height = $this->getSize('height');
		$this->zindex = $this->getInt('zindex');
		$this->addGenOption("zindex : ".($this->zindex+20) );
		
		$float_position = $this->getString('float_position');
		if($float_position!='left' && $float_position!='right') $float_position = '';
		$mode = $this->getInt('mode');
		switch($mode){
			case 0: 
					$this->addGenOption("horiz : 0");
					$this->addGenOption("col : 	1" );
					$this->addGenOption("fade : 0" );
					$this->insertString( 'effect' );
				break;
			case 1: 
					$this->addGenOption("horiz : 1");
					$this->addGenOption("col : 	1" );
					$this->addGenOption("fade : 0" );
					$this->insertString( 'effect' );
				break;
			case 2: 
					$this->addGenOption("horiz : 0");
					$this->addGenOption("fade : 0" );
					$this->insertString( 'effect' );
					$this->insertString( 'col' );
				break;
			case 3: 
					$this->addGenOption("fade : 1" );
				break;
		}
		$this->top_pad 		+= $this->getInt('pending_all_top', 0);
		$this->left_pad		+= $this->getInt('pending_all_left', 0);
		$this->right_pad	+= $this->getInt('pending_all_right', 0);
		$this->bottom_pad	+= $this->getInt('pending_all_bottom', 0);
		$this->insertInt('show_pause');
		$this->insertInt('tdelay');
		$this->insertInt('timer');
		$this->insertInt('duration');
		$this->debug = $this->getString('debug');
		$this->addGenOption("debug : ".$this->debug);
		$activePositions = '';
		if(!$this->render_content){
			$this->setPosition( 'prev_big');
			$this->setPosition( 'next_big');
			$this->setPosition( 'prev');
			$this->setPosition( 'menu', $this->menu);
			$this->setPosition( 'play');
			$this->setPosition( 'next');
			$activePositions = $this->getActivePositions();
		}
		
		if ( $this->slider_html && ( $this->multitag || count($this->list) ) ){
				$return_text .= $this->getString('pretext');
				
				
				if($this->getInt('type_all_bg_color')==2) $all_color_style = $this->getGradient('all_bg_color_start', 'all_bg_color_end');
					elseif($this->getInt('type_all_bg_color')) $all_color_style = 'background-color: '.$this->getColor('all_bg_color').';';
						else $all_color_style = '';
				
				$return_text .= '<div  id="twoj_slider_block_id'.$this->uniqueid.'" class="twoj_slider_block'.($this->block_class?$this->block_class:'').($this->debug?' twoj_slider_debug':'').'" style="'
				.'overflow: hidden; '
				.'position: relative; '
				.'padding-top: '.($this->top_pad).'px; '
				.'padding-left: '.($this->left_pad).'px; '
				.'padding-right: '.($this->right_pad).'px; '
				.'padding-bottom: '.($this->bottom_pad).'px; '
				.'z-index:'.$this->zindex.'; '
				.'width: '.$width.'; '
				.'height: '.$height.'; '
				.($float_position?'float:'.$float_position.';':'')
				.($this->debug?'border: 1px solid fuchsia;':'')
				.$all_color_style
				.'">';
				if( $this->getInt('timer_indicator', 0) ) $return_text .= '<div id="twoj_slider_timer_indication_id'.$this->uniqueid.'"  	class="twoj_slider_timer_indication"  style="z-index:'.($this->zindex+10).'; width: 0px;"></div>';
				
				if(!$this->render_content) $return_text .= $activePositions ;
				
				$return_text .= $this->slider_html;
				if( $this->getString('text_pause', '') ) $return_text .= '<div id="twoj_slider_pause_block_id'.$this->uniqueid.'" style="display:none;">'.$this->getString('text_pause').'</div>';
				$return_text .='</div>';
				if(!$this->render_content){
					$return_text .= '<script language="JavaScript" type="text/javascript"> 
					<!--//<![CDATA[
					'.( $this->contentImages==2? 'emsajax( "#twoj_slider_block_id'.$this->uniqueid.' img" ).TwojImageResize({ height: '.$this->getInt('imagesHeight', 0).', width: '.$this->getInt('imagesWidth', 0).' });' : '' ).'
					emsajax(document).ready(function(){ emsajax("#twoj_slider_block_id'.$this->uniqueid.'").twoJSlider({'.implode(' ,', $this->gen_option).'}); });
					//]]>-->
					</script>';
				}
				$return_text .= $this->getString('posttext');
				$return_text .= $this->debug?'<div id="log_events"></div>':'';
		}
		if($return_text) return  $return_text; else return null;
	}
	
	function articles_diplay(){
		$ret_html = '';

		$this->menu.='<div id="twoj_slider_menu_id'.$this->uniqueid.'" class="twoj_slider_menu" style="'.($this->getString('menu')=='-'?'display:none;':'').'z-index:'.($this->zindex+4).'">';
		
		$border = '';
		if( $this->getInt('type_border') &&  $this->getString('border_s') && $this->getString('border_s') != 'none'){
			$border .= 'border:'.$this->getSize( 'border_w', null, 0).' '.$this->getString( 'border_s').' '.$this->getColor( 'border_color' ).';';
		}
		if( $this->getInt('type_border')==2 &&  $this->getString('shadow_w')){
       		if( $this->browser->getBrowser()=='msie' &&  $this->browser->getMajor()<9){
				$border .= 'outline: '.$this->getSize( 'shadow_w', null, 0).' solid '.$this->getColor( 'shadow_color' ).';';
			} else {
				$border .= 'box-shadow: 0px 0px 0px '.$this->getSize( 'shadow_w', null, 0).' '.$this->getColor( 'shadow_color' ).';';
			}
			$this->top_pad  	+= $this->getInt('shadow_w');
			$this->left_pad 	+= $this->getInt('shadow_w');
			$this->right_pad 	+= $this->getInt('shadow_w');
			$this->bottom_pad 	+= $this->getInt('shadow_w');
		}
		
		$color_style = '';
		$type_bg_color = $this->getInt('type_bg_color');
		if($type_bg_color==1){
			$color_style .='background: '.$this->getColor('bg_color').'; filter: \'\';';
		}
		if($type_bg_color=0){
			$color_style .= $this->getGradient('bg_color_start', 'bg_color_end');
		}
		$ret_html.='<div  id="twoj_slider_outter_id'.$this->uniqueid.'"  class="twoj_slider_outter"  style="'.$color_style.$border.' z-index:'.($this->zindex+1).'; margin:0; padding:0;">';
		$ret_html.='<div class="twoj_slider_inner_ul" id="twoj_slider_inner_ul_id'.$this->uniqueid.'" style="z-index:'.($this->zindex+2).';">';
		$slide_number = 0;
		if($this->multitag){
			for($i=0;$i<count($this->page_array);$i++){
				$page = $this->page_array[$i];
				$ret_html.='<div class="twoj_slider_inner_li" style="z-index:'.($this->zindex+3).';">'.$page->conten.'</div>';
				$this->menu .= '<div class="twoj_slider_menu_link" style="'.($this->getInt('show_label')?'':'text-indent: -9999px; ').'z-index:'.($this->zindex+5).';">'.(++$slide_number).'</div>'; //$page->title
			}
		} else {
			$this->prepare_template();
			if ($this->grouped){
				foreach ($this->list as $group_name => $group){
					$this->menu .= '<div class="twoj_slider_menu_link" style="'.($this->getInt('show_label')?'':'text-indent: -9999px; ').'z-index:'.($this->zindex+5).';">'.(++$slide_number).'</div>';//$group_name
					$ret_html.="\n".'<div class="twoj_slider_inner_li" style="z-index:'.($this->zindex+3).';">';
					foreach ($group as $item){						
						$ret_html.= $this->article_diplay($item);
					}
					$ret_html.='</div>'."\n";
					
				}
			} else {
				foreach ($this->list as $item){
					$this->menu .= '<div class="twoj_slider_menu_link" style="'.($this->getInt('show_label')?'':'text-indent: -9999px; ').'z-index:'.($this->zindex+5).';">'.(++$slide_number).'</div>';//$item->title
					$ret_html.="\n".'<div class="twoj_slider_inner_li" style="z-index:'.($this->zindex+3).';">';
					$ret_html.= $this->article_diplay($item);
					$ret_html.='</div>'."\n";
				}
			}
		}
		$this->menu.='</div>';
		$ret_html.='</div>';
		$ret_html.='</div>';
		return $ret_html;
	}
	function prepare_template(){
		if($this->getInt('enable_template') && $this->getString('content_template') ){
			$this->content_template = $this->getString('content_template');
		} else {
			$show_hits 		= $this->getInt('show_hits', 0);
			$show_author 	= $this->getInt('show_author', 0);
			$show_category 	= $this->getInt('show_category', 0);
			$link_titles 	= $this->getInt('link_titles', 0);
			
			$show_date_create 		= $this->getInt('show_date_create', 0);
			$show_date_modified 	= $this->getInt('show_date_modified', 0);
			$show_date_publish_up 	= $this->getInt('show_date_publish_up', 0);
			
			if(!$this->getInt('show_titles')){
				$item_heading = str_replace( array('/', '<', '>', ' '), '', $this->getString('item_heading', 'h3'));
				if($item_heading=='') $item_heading = 'h3';
				$this->content_template .= '<'.$item_heading.'>';
				if($link_titles) $this->content_template .= '<a href="@READMORELINK@">';
				$this->content_template .= '@TITLE@';
				if($link_titles) $this->content_template .= '</a>';
				$this->content_template .='</'.$item_heading.'>';
			}
			
			$cat_tag = ($this->getInt('link_category',0)?'@CATEGORYLINK@':'@CATEGORY@');
			
			if($show_hits || $show_author || $show_category || $show_date_create || $show_date_modified || $show_date_publish_up){
				$this->content_template .= '<dl class="article-info" style="clear: both;">';
				if($show_category) 			$this->content_template .= '<dd class="category-name">'.$cat_tag.'</dd>';
				if($show_date_create) 		$this->content_template .= '<dd class="create">@CREATEDATE@</dd>';
				if($show_date_modified) 	$this->content_template .= '<dd class="modified">@MODIFIEDDATE@</dd>';
				if($show_date_publish_up) 	$this->content_template .= '<dd class="published">@PUBLISHDATE@</dd>';
				if($show_author)			$this->content_template .= '<dd class="createdby">@AUTHOR@</dd>';
				if($show_hits ) 			$this->content_template .= '<dd class="hits">@HITS@</dd>';
				$this->content_template .= '</dl>';
			}
			
			if ($this->params->get('show_introtext')) $this->content_template .= '@TEXT@';
			if ($this->params->get('show_readmore') ) $this->content_template .= '@READMORE@';
		}
	}
	
	
	
	
	function article_diplay($item){
		$return_html='';
		
		$text_title = $item->title;
		
		//category text
		$category_url = '<a href="'.JRoute::_(ContentHelperRoute::getCategoryRoute($item->catslug)).'">'.$item->category_title.'</a>';
		$cat_text = JText::sprintf('COM_CONTENT_CATEGORY', $item->category_title); 
		if ($item->catslug)
			$cat_text_url = JText::sprintf('COM_CONTENT_CATEGORY', $category_url); 
			else  $cat_text_url = JText::sprintf('COM_CONTENT_CATEGORY', $item->category_title); 
		
		$date_create = JText::sprintf('COM_CONTENT_CREATED_DATE_ON', JHtml::_('date',$item->created, $this->date_format));
		$date_modified = JText::sprintf('COM_CONTENT_LAST_UPDATED', JHtml::_('date',$item->modified, $this->date_format));
		$date_publish_up = JText::sprintf('COM_CONTENT_PUBLISHED_DATE_ON', JHtml::_('date',$item->publish_up, $this->date_format));
		
		$text_author = JText::sprintf('COM_CONTENT_WRITTEN_BY', ($item->created_by_alias ? $item->created_by_alias : $item->author) );
		$text_hits = JText::sprintf('COM_CONTENT_ARTICLE_HITS', $item->hits);
		
		$text_link = $item->link;
	
		if($item->readmore){
			$text_readmore='<p class="readmore"><a href="'.$item->link.'">';
			if ($item->params->get('access-view')== FALSE){
				$text_readmore.=JText::_('COM_CONTENT_REGISTER_TO_READ_MORE');
			} else {
				if ( ( $readmore = $item->alternative_readmore) && $this->params->get('show_readmore_title', 0) == 1 ){
					$text_readmore.=$readmore.' ';
					$text_readmore.=JHtml::_('string.truncate', $item->title, $this->params->get('readmore_limit'));
				} elseif ($this->params->get('show_readmore_title', 0) == 0){
					$text_readmore.=JText::sprintf('COM_CONTENT_READ_MORE_TITLE');
				} else {
					$text_readmore.=JText::_('COM_CONTENT_READ_MORE').' ';
					$text_readmore.=JHtml::_('string.truncate', $item->title, $this->params->get('readmore_limit'));
				}
			}
			$text_readmore.='</a></p>';
		} else $text_readmore = '';
		
		$text_text =  $item->displayIntrotext;
		
		$copy_template = $this->content_template;

		$copy_template = str_replace( 
			array('@TITLE@', 	'@TEXT@', 	'@READMORE@', 	'@READMORELINK@', 	'@CATEGORY@', 	'@CATEGORYLINK@', 	'@CREATEDATE@', '@MODIFIEDDATE@', 	'@PUBLISHDATE@', 	'@AUTHOR@', 	'@HITS@'),
			array($text_title,	$text_text,	$text_readmore,	$text_link,			$cat_text,		$cat_text_url,		$date_create,	$date_modified,		$date_publish_up,	$text_author,	$text_hits),
			$copy_template
		);
		
		$return_html.= $copy_template;
		return $return_html;
	}
	
}
